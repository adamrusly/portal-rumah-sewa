<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class NonResidentController extends Controller
{
    public function listingHouse(Request $request)
    {
        $listing = [
        	[
        		"name" => "Shah Alam",
        		"desc" => "Shah Alam"

        	],
        	[
        		"name" => "Kuala Lumpur",
        		"desc" => "Kuala Lumpur"

        	],
        ];
        return response()->json($listing);
    }
    public function paparListingHouse(Request $request)
    {
        $listing = [
        	[
        		"name" => "Shah Alam",
        		"desc" => "Shah Alam"

        	],
        	[
        		"name" => "Kuala Lumpur",
        		"desc" => "Kuala Lumpur"

        	],
        ];
        return view('papar_listing',[]);
    }
}
